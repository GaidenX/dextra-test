const express = require('express');
// force https on online page
const app = express().use(function (req, res, next) {
	if (req.header('x-forwarded-proto') == 'http') {
		res.redirect(301, 'https://caioamericodextra.herokuapp.com' + req.url)
		return
	}
	next()
});
const server = require('https').createServer(app);
const handleBars = require('express-handlebars');
const path = require('path');
const cookieParser = require('cookie-parser');
const porta = process.env.PORT || 8080;

// General Setup
app.use(express.json());
app.use(express.urlencoded({
	extended: true
}));
app.use(cookieParser());
app.use(express.static(__dirname + '/public'));
//---

// HandleBars - Template Engine
app.set('views', path.join(__dirname, '/views'));
app.engine('handlebars', handleBars({
	defaultLayout: "index"
}));
app.set('view engine', 'handlebars');
//---

// Required Routes
const sqlConnection = require('./routes/db');
const conSql = sqlConnection.sqlConfig();
const cartActions = require('./routes/cart-actions');
//---

/** Pages Routes **/
app.get('/', function (req, res) {
    sqlConnection.getIngredients().then(data => {
          res.render('home', {
            style: ['/css/main.css'],
            javascript: ['js/shop.js','js/cart.js','js/main.js'],
            products: ['XBacon','XBurguer','XEgg','XEggBacon'],
            ingredients: [
                {name:data[0].name, price: data[0].price},
                {name:data[1].name, price: data[1].price},
                {name:data[2].name, price: data[2].price},
                {name:data[3].name, price: data[3].price},
                {name:data[4].name, price: data[4].price}
            ],
            cart: req.cookies.cart
        });
     }).catch(err => {
        res.render('home', {
            style: ['/css/main.css'],
            javascript: ['js/shop.js','js/cart.js','js/main.js'],
            products: ['XBacon','XBurguer','XEgg','XEggBacon'],
            ingredients: [
                {name:"alface",price:0.4},
                {name:"bacon",price:2.0},
                {name:"hamburguer",price:3.0},
                {name:"ovo",price:0.8},
                {name:"queijo",price:1.5},
            ],
            cart: req.cookies.cart
        });
    });
});
/** END Routes **/

/** Cart Routes API **/
app.post('/addCart/', function (req, res) {
    cartActions.addCart(req, res);
});

app.post('/removeCart/', function (req, res) {
    cartActions.removeCart(req, res);
});
/** END Cart Routes API **/

/** 404 redirect to home file - DONT HAVE 404 Page Html **/
app.get('*', function (req, res) {
    res.render('home', {
        style: ['/css/main.css'],
        javascript: ['js/shop.js','js/cart.js','js/main.js'],
        products: ['XBacon','XBurguer','XEgg','XEggBacon'],
        ingredients: [
            {name:"alface",price:0.4},
            {name:"bacon",price:2.0},
            {name:"hamburguer",price:3.0},
            {name:"ovo",price:0.8},
            {name:"queijo",price:1.5},
        ],
        cart: req.cookies.cart
    });
});
/** END 404 **/

app.listen(porta, function () {
	console.log("server on in: " + porta)
});