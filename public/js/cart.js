// Cart Constructor, Cart Actions and Updates
// On open check if cookie cart already exist and Update the cart
(function () {
    if (typeof $.cookie('cart') != "undefined" || $.cookie('cart') != null) {
        var cartList = $.parseJSON($.cookie('cart').replace('j:', ''));
        updateCart(cartList);
    }
})();

// Update Cart and constructor with de informations
function updateCart(cartList) {
    let listCartItems = cartList.info;

    var tableContent = '';
    if (listCartItems.length <= 0)
        tableContent = '<tr class="tr-shadow"><td align="center" colspan="4">Seu carrinho está atualmente vazio</td></tr>';
    else
        for (var key in listCartItems) {
            tableContent += '<tr class="tr-shadow" id="' + listCartItems[key].idToCart + '"><td>' + listCartItems[key].snackType + '</td>' +
                '<td><ul>' +
                '<li>' + listCartItems[key].alfaceQtd + 'x Alface</li>' +
                '<li>' + listCartItems[key].baconQtd + 'x Bacon</li>' +
                '<li>' + listCartItems[key].hamburguerQtd + 'x Hamburguer de Carne</li>' +
                '<li>' + listCartItems[key].ovoQtd + 'x Ovo</li>' +
                '<li>' + listCartItems[key].queijoQtd + 'x Queijo</li>' +
                '</ul></td>' +
                '<td><span class="block-product">R$ ' + listCartItems[key].priceTotal.replace('.', ',') + '</span></td>' +
                '<td><button class="buttonRemoveCart" data-placement="top" title="Remover" onclick="removeProdCart(' + listCartItems[key].idToCart + ')"><i class="fas fa-minus"></i></button></td>'
            '</tr><tr class="spacer"></tr>';
        }
    $('.cartTable').html(tableContent);
    showFinishOrder(listCartItems)
}

// Request to remove item from the Cart/Cookie and after UpdateCart
function removeProdCart(idProd) {
    $.ajax({
        url: '/removeCart',
        type: 'post',
        data: {
            cartProdId: idProd
        },
        success: function (data) {
            console.log(data.status);
            var cartList = $.parseJSON($.cookie('cart').replace('j:', ''));
            updateCart(cartList);
        },
        error: function (err) {
            console.log(err);
        }
    })
}

// Logic to show the Finish Order Area
function showFinishOrder(data) {
    if (data.length > 0 && $(".finish-order").is(":visible") === false) {
        $(".finish-order").slideToggle("fast");
    } else if (data.length == 0 && $(".finish-order").is(":visible")) {
        $(".finish-order").slideToggle("fast");
    }
}