// Start the Needed Functions and listeners
(function () {
    //Starts the constructor to page and the shopping listener/Logics
    snacksConstructor()
    ingredientsConstructor()
    Shop.shopping();
    //---

    $('#addProd').on('submit', function (event) {
        event.preventDefault();
        Shop.generateShoppingId();

        $.ajax({
            url: $(this).attr('action'),
            type: 'post',
            data: $(this).serialize(),
            success: function (data, textStatus) {
                console.log(data);
                $('#addProd')[0].reset();
                updateCart(data);
            },
            error: function (err) {
                $(this).reset();
                console.log(err);
            }
        })
    });

    $('.priceTotal').change(function () {
        promotionsContructor(Shop.promotions);
    });

    $('#addProd').on('reset', function (event) {
        $('.section-ingredients').hide(500);
        $('.card-prods').removeClass('card-checked');
        $('.promotion-label').removeClass('active');
    });
})();

// Constructor of the Snacks Section / logics of interface
function snacksConstructor() {
    $('.card-prods').on('click', function () {
        $(this).find('input[type="radio"]').prop('checked', true).change();
        $('.card-prods').removeClass('card-checked');
        $(this).addClass('card-checked');
        $('.section-ingredients').show(500);
    });

    $(".card-snacks").each(function (index) {
        var snackType = $(this).attr('data-snack-type');
        var listOfIngredients = Shop.listSnacks(snackType);
        var html = '';
        for (var key in listOfIngredients)
            html += '<li>' + listOfIngredients[key] + '</li>'

        $(this).children('ul').html(html);
    });

    $('input[type=radio][name="snackType"]').change(function () {
        var listOfIngredients = Shop.listSnacks(this.value);
        $('.input-qtd').val(0).change();
        for (var key in listOfIngredients)
            $('.input-qtd[data-ingredient=' + listOfIngredients[key] + ']').val(1).change();
    });
}

// Constructor of the Ingredients Section / logics of interface
function ingredientsConstructor() {
    let price = 0;

    $('.btn-number').click(function (e) {
        e.preventDefault();

        var fieldName = $(this).attr('data-field');
        var type = $(this).attr('data-type');
        var input = $("input[name='" + fieldName + "']");

        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if (type == 'minus') {
                if (currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
            } else if (type == 'plus') {
                input.val(currentVal + 1).change();
            }
        } else {
            input.val(0).change();
        }
    });
}

// Constructor of the Promotions Labels / logics of interface
function promotionsContructor(avaliablePromotions){
    (avaliablePromotions.Light) ? $('.promotion-light').addClass('active') :  $('.promotion-light').removeClass('active');
    (avaliablePromotions.MuitaCarne) ? $('.promotion-steak').addClass('active') :  $('.promotion-steak').removeClass('active');
    (avaliablePromotions.MuitoQueijo) ? $('.promotion-cheese').addClass('active') :  $('.promotion-cheese').removeClass('active');
}