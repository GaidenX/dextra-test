// Class with the data and logics to do all the shopping logics and action
// Store All Products, All Promotions and the shopping action here
var Shop = {
    listSnacks: function (snackName) {
        let ingredients = [];

        if (snackName == "XBacon") ingredients = ['bacon', 'hamburguer', 'queijo']
        if (snackName == "XBurguer") ingredients = ['hamburguer', 'queijo']
        if (snackName == "XEgg") ingredients = ['ovo', 'hamburguer', 'queijo']
        if (snackName == "XEggBacon") ingredients = ['ovo', 'bacon', 'hamburguer', 'queijo']

        return ingredients;
    },
    promotions: {
        'Light': false,
        'MuitaCarne': false,
        'MuitoQueijo': false
    },
    generateShoppingId: function () {
        // Generate a random Id to this Shopping Time
        $('#idToCart').val(Math.floor(Math.random() * 1000))
    },
    shopping: function () {
        $('.input-qtd').change(function () {
            // All promotion types to check and set status
            Shop.promotions.MuitaCarne = ($('#hamburguerQtd').val() >= 3) ? true : false;
            Shop.promotions.MuitoQueijo = ($('#queijoQtd').val() >= 3) ? true : false;
            Shop.promotions.Light = ($('#alfaceQtd').val() > 0 && $('#baconQtd').val() == 0) ? true : false;
            // ---

            // Change the current amount price of each item
            var inputTotal = $(".priceItem[data-ingredient='" + $(this).attr('data-ingredient') + "']");
            var newTotal = $(this).attr('data-price') * $(this).val();
            inputTotal.val(newTotal.toFixed(2)).change();
            // ---

            // MuitaCarne Promotion && MuitoQueijo Promotion Logic
            if ($(this).attr('data-ingredient') == 'hamburguer' || $(this).attr('data-ingredient') == 'queijo') {
                if ($(this).val() >= 3) {
                    var inputValue = $('.priceItem[data-ingredient="' + $(this).attr('data-ingredient') + '"]').val();
                    inputValue -= $(this).attr('data-price') * parseInt($(this).val() / 3);
                    $('.priceItem[data-ingredient="' + $(this).attr('data-ingredient') + '"]').val(inputValue.toFixed(2)).change();
                }
            }
            // ---

            // Light Promotion Logic
            if (Shop.promotions.Light) {
                var totalValue = $('#priceTotal').val();
                totalValue -= ($('#priceTotal').val() * 0.1);
                $('#priceTotal').val(totalValue.toFixed(2)).change();
            }
            // ---
        });

        $('.priceItem').change(function () {
            price = 0;
            $(".priceItem").each(function () {
                price += parseFloat($(this).val());
            })
            $('.priceTotal').val(price.toFixed(2)).change();
        });
    }
};