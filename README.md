# Dextra Test - Caio Americo

Projeto de Teste para Dextra

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Especificações

* Server-side > NodeJs, HandleBars Engine Template.
* Client-side > HandleBars, HTML, CSS, Bootstrap 4, JavaScript/jQuery.
* Banco de dados MySql, Tabela > todo/Dextra.

Recomendações para instalação do projeto:
```
Tenha NodeJs instalado na máquina
MySQL Workbench para vizualização e acesso ao banco
```

### Installing

[OPCIONAL] Primeiramente tenha acesso ao banco de dados MySql e a tabela Dextra com os produtos cadastrados na aplicação.

Credenciais do Banco:
```
host: "mysql995.umbler.com",
port: 41890,
user: "caioamerico",
password: "lalaop13",
database: "todo"
```

Baixe os arquivos do git, abra o Terminal/Cmd dentro do projeto e execute geral o localhost:
```
node index.js
```

Agora basta acessar o localhost do projeto.
```
http://localhost:8080/
```

## Deploy

Deploy em servidores da Heroku App:
[Versão em Live](https://caioamericodextra.herokuapp.com/)

## Build

* [Heroku App CLI NodeJs](https://devcenter.heroku.com/articles/heroku-cli)
