const express = require('express');
const app = express();

exports.addCart = function(req, res){
    try {
        let cart = {
            info: []
        };
        cart.info.push(req.body);

        if (typeof req.cookies.cart != "undefined") {
            for (var key in req.cookies.cart.info) {
                cart.info.push(req.cookies.cart.info[key])
            }
        }

        res.cookie('cart', cart);
        res.status(200).send(cart);
    } catch (err) {
        res.status(404).send(err);
    }
}

exports.removeCart = function(req, res){
    try {
        for (var key in req.cookies.cart.info) {
            if (req.cookies.cart.info[key].idToCart == req.body.cartProdId) {
                req.cookies.cart.info.splice(key, 1);
            }
        }
        res.cookie('cart', req.cookies.cart);
        res.status(200).send({
            status: 'Produto removido do carrinho com sucesso'
        });
    } catch (err) {
        res.status(404).send(err);
    }
}